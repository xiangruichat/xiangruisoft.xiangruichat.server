﻿using Aiursoft.Pylon;
using Aiursoft.Pylon.Attributes;
using Aiursoft.Pylon.Exceptions;
using Aiursoft.Pylon.Models;
using Aiursoft.Pylon.Models.ForApps.AddressModels;
using Aiursoft.Pylon.Models.Stargate.ListenAddressModels;
using Aiursoft.Pylon.Services;
using Aiursoft.Pylon.Services.ToAPIServer;
using Aiursoft.Pylon.Services.ToStargateServer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using StackExchange.Redis;
using XiangruiSoft.XiangruiChat.Server.Data;
using XiangruiSoft.XiangruiChat.Server.Models;
using XiangruiSoft.XiangruiChat.Server.Services;
using XiangruiSoft.XiangruiChat.Server.ViewModels;

namespace XiangruiSoft.XiangruiChat.Server.Controllers
{
    [APIExpHandler]
    [APIModelStateChecker]
    public class AuthController : Controller
    {
        private readonly ServiceLocation _serviceLocation;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _env;
        private readonly AuthService<XiangruiChatUser> _authService;
        private readonly OAuthService _oauthService;
        private readonly UserManager<XiangruiChatUser> _userManager;
        private readonly SignInManager<XiangruiChatUser> _signInManager;
        private readonly UserService _userService;
        private readonly AppsContainer _appsContainer;
        private readonly PushChatMessageService _pusher;
        private readonly ChannelService _channelService;
        private readonly VersionChecker _version;
        private readonly ApplicationDbContext _dbContext;

        public AuthController(
            ServiceLocation serviceLocation,
            IConfiguration configuration,
            IHostingEnvironment env,
            AuthService<XiangruiChatUser> authService,
            OAuthService oauthService,
            UserManager<XiangruiChatUser> userManager,
            SignInManager<XiangruiChatUser> signInManager,
            UserService userService,
            AppsContainer appsContainer,
            PushChatMessageService pusher,
            ChannelService channelService,
            VersionChecker version,
            ApplicationDbContext dbContext)
        {
            _serviceLocation = serviceLocation;
            _configuration = configuration;
            _env = env;
            _authService = authService;
            _oauthService = oauthService;
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _appsContainer = appsContainer;
            _pusher = pusher;
            _channelService = channelService;
            _version = version;
            _dbContext = dbContext;
        }

        public async Task<IActionResult> Version()
        {
            var latest = await _version.Check();
            return this.XiangruiJson(new VersionViewModel
            {
                LatestVersion = latest,
                OldestSupportedVersion = latest,
                Message = "成功获取应用版本.",
                DownloadAddress = _configuration["ChatMasterPackageJson"]
            });
        }

        [HttpPost]
        public async Task<IActionResult> ReportBug(ReportBugAddressModel model)
        {
            HttpClient client = new HttpClient();
            await client.GetAsync($"http://sc.ftqq.com/{_configuration["ServerChanKey"]}.send?text=Bug反馈重复1&desp={model.Content}");
            await client.GetAsync($"http://sc.ftqq.com/{_configuration["ServerChanKey"]}.send?text=Bug反馈重复2&desp={model.Content}");
            return this.XiangruiJson(new AiurProtocal()
            {
                Code = ErrorType.Success,
                Message = "反馈错误成功!"
            });
        }

        [HttpPost]
        public async Task<IActionResult> ReportMessage(ReportMessageAddressModel model)
        {
            HttpClient client = new HttpClient();
            await client.GetAsync($"http://sc.ftqq.com/{_configuration["ServerChanKey"]}.send?text=需求反馈&desp={model.Content}");
            return this.XiangruiJson(new AiurProtocal()
            {
                Code = ErrorType.Success,
                Message = "反馈需求成功!"
            });
        }

        [HttpPost]
        public async Task<IActionResult> AuthByPassword(AuthByPasswordAddressModel model)
        {
            var pack = await _oauthService.PasswordAuthAsync(Extends.CurrentAppId, model.Email, model.Password);
            if (pack.Code != ErrorType.Success)
            {
                return this.Protocal(ErrorType.Unauthorized, pack.Message);
            }
            var user = await _authService.AuthApp(new AuthResultAddressModel
            {
                code = pack.Value,
                state = string.Empty
            }, isPersistent: true);
            if (!await _dbContext.AreFriends(user.Id, user.Id))
            {
                _dbContext.AddFriend(user.Id, user.Id);
                await _dbContext.SaveChangesAsync();
            }
            return this.XiangruiJson(new AiurProtocal()
            {
                Code = ErrorType.Success,
                Message = "授权成功."
            });
        }


        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserAddressModel model)
        {
            var result = await _oauthService.AppRegisterAsync(model.Email, model.Password, model.ConfirmPassword);
            return this.XiangruiJson(result);
        }

        public async Task<IActionResult> SignInStatus()
        {
            var user = await GetXiangruiChatUser();
            var signedIn = user != null;
            return this.XiangruiJson(new AiurValue<bool>(signedIn)
            {
                Code = ErrorType.Success,
                Message = "成功获取登录状态."
            });
        }

        [AiurForceAuth(directlyReject: true)]
        public async Task<IActionResult> Me()
        {
            var user = await GetXiangruiChatUser();
            return this.XiangruiJson(new AiurValue<XiangruiChatUser>(user)
            {
                Code = ErrorType.Success,
                Message = "成功获取用户信息."
            });
        }

        [HttpPost]
        [AiurForceAuth(directlyReject: true)]
        public async Task<IActionResult> UpdateInfo(UpdateInfoAddressModel model)
        {
            var cuser = await GetXiangruiChatUser();
            cuser.HeadImgFileKey = model.HeadImgKey;
            cuser.NickName = model.NickName;
            cuser.Bio = model.Bio;
            await _userService.ChangeProfileAsync(cuser.Id, await _appsContainer.AccessToken(), cuser.NickName, cuser.HeadImgFileKey, cuser.Bio);
            await _userManager.UpdateAsync(cuser);
            return this.Protocal(ErrorType.Success, "成功上传用户信息.");
        }

        [HttpPost]
        [AiurForceAuth(directlyReject: true)]
        public async Task<IActionResult> ChangePassword(ChangePasswordAddresModel model)
        {
            var cuser = await GetXiangruiChatUser();
            var result = await _userService.ChangePasswordAsync(cuser.Id, await _appsContainer.AccessToken(), model.OldPassword, model.NewPassword);
            return this.Protocal(ErrorType.Success, "修改密码成功!");
        }


        [AiurForceAuth(directlyReject: true)]
        public async Task<IActionResult> InitPusher()
        {
            var user = await GetXiangruiChatUser();
            if (user.CurrentChannel == -1 || (await _channelService.ValidateChannelAsync(user.CurrentChannel, user.ConnectKey)).Code != ErrorType.Success)
            {
                var channel = await _pusher.Init(user.Id);
                user.CurrentChannel = channel.ChannelId;
                user.ConnectKey = channel.ConnectKey;
                await _userManager.UpdateAsync(user);
            }
            var model = new InitPusherViewModel
            {
                Code = ErrorType.Success,
                Message = "成功初始化您的 Websocket 监听Channel",
                ChannelId = user.CurrentChannel,
                ConnectKey = user.ConnectKey,
                ServerPath = new AiurUrl(_serviceLocation.StargateListenAddress, "Listen", "Channel", new ChannelAddressModel
                {
                    Id = user.CurrentChannel,
                    Key = user.ConnectKey
                }).ToString()
            };
            return this.XiangruiJson(model);
        }

        [AiurForceAuth(directlyReject: true)]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return this.Protocal(ErrorType.Success, "登出成功.");
        }

        private async Task<XiangruiChatUser> GetXiangruiChatUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            return await _userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}
