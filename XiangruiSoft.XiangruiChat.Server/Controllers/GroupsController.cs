﻿using Aiursoft.Pylon;
using Aiursoft.Pylon.Attributes;
using Aiursoft.Pylon.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using XiangruiSoft.XiangruiChat.Server.Data;
using XiangruiSoft.XiangruiChat.Server.Models;
using XiangruiSoft.XiangruiChat.Server.Services;
using XiangruiSoft.XiangruiChat.Server.ViewModels;

namespace XiangruiSoft.XiangruiChat.Server.Controllers
{
    [APIExpHandler]
    [APIModelStateChecker]
    [AiurForceAuth(directlyReject: true)]
    public class GroupsController : Controller
    {
        private readonly UserManager<XiangruiChatUser> _userManager;
        private readonly SignInManager<XiangruiChatUser> _signInManager;
        private readonly ApplicationDbContext _dbContext;

        public GroupsController(
            UserManager<XiangruiChatUser> userManager,
            SignInManager<XiangruiChatUser> signInManager,
            ApplicationDbContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext;
        }

        public async Task<IActionResult> SearchGroup(SearchGroupAddressModel model)
        {
            var groups = await _dbContext
                .GroupConversations
                .AsNoTracking()
                .Where(t => t.GroupName.Contains(model.GroupName, StringComparison.CurrentCultureIgnoreCase))
                .Take(model.Take)
                .ToListAsync();

            return this.XiangruiJson(new AiurCollection<GroupConversation>(groups)
            {
                Code = ErrorType.Success,
                Message = "搜索结果已经返回."
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateGroupConversation(CreateGroupConversationAddressModel model)
        {
            var user = await GetXiangruiChatUser();
            model.GroupName = model.GroupName.Trim().ToLower();
            var exsists = _dbContext.GroupConversations.Any(t => t.GroupName == model.GroupName);
            if (exsists)
            {
                return this.Protocal(ErrorType.NotEnoughResources, $"这个: {model.GroupName} 已经重复!");
            }
            var limitedDate = DateTime.UtcNow - new TimeSpan(1, 0, 0, 0);
            var todayCreated = await _dbContext
                .GroupConversations
                .Where(t => t.OwnerId == user.Id)
                .Where(t => t.ConversationCreateTime > limitedDate)
                .CountAsync();
            if (todayCreated > 20)
            {
                return this.Protocal(ErrorType.NotEnoughResources, "今天创建的群过多，请明天再试!");
            }
            var createdGroup = await _dbContext.CreateGroup(model.GroupName, user.Id, model.JoinPassword);
            var newRelationship = new UserGroupRelation
            {
                UserId = user.Id,
                GroupId = createdGroup.Id,
                ReadTimeStamp = DateTime.MinValue
            };
            _dbContext.UserGroupRelations.Add(newRelationship);
            await _dbContext.SaveChangesAsync();
            return this.XiangruiJson(new AiurValue<int>(createdGroup.Id)
            {
                Code = ErrorType.Success,
                Message = "您已经创建群组并且加入了她!"
            });
        }

        [HttpPost]
        public async Task<IActionResult> JoinGroup([Required]string groupName, string joinPassword)
        {
            var user = await GetXiangruiChatUser();
            var group = await _dbContext.GroupConversations.SingleOrDefaultAsync(t => t.GroupName == groupName);
            if (group == null)
            {
                return this.Protocal(ErrorType.NotFound, $"无法找到这个群聊: {groupName}!");
            }
            var joined = _dbContext.UserGroupRelations.Any(t => t.UserId == user.Id && t.GroupId == group.Id);
            if (joined)
            {
                return this.Protocal(ErrorType.HasDoneAlready, $"您已经在这个群聊里了 {groupName}!");
            }
            if (group.HasPassword && group.JoinPassword != joinPassword?.Trim())
            {
                return this.Protocal(ErrorType.WrongKey, "这个群聊需要密码，但是您的密码并不正确!");
            }
            // All checked and able to join him.
            // Warning: Currently we do not have invitation system for invitation control is too complicated.
            var newRelationship = new UserGroupRelation
            {
                UserId = user.Id,
                GroupId = group.Id
            };
            _dbContext.UserGroupRelations.Add(newRelationship);
            await _dbContext.SaveChangesAsync();
            return this.Protocal(ErrorType.Success, $"您已经成功加入这个群聊: {groupName}!");
        }

        [HttpPost]
        public async Task<IActionResult> LeaveGroup([Required]string groupName)
        {
            var user = await GetXiangruiChatUser();
            var group = await _dbContext.GroupConversations.SingleOrDefaultAsync(t => t.GroupName == groupName);
            if (group == null)
            {
                return this.Protocal(ErrorType.NotFound, $"无法找到这个群聊: {groupName}!");
            }
            var joined = await _dbContext.GetRelationFromGroup(user.Id, group.Id);
            if (joined == null)
            {
                return this.Protocal(ErrorType.HasDoneAlready, $"您没有加入这个群聊： {groupName}！");
            }
            _dbContext.UserGroupRelations.Remove(joined);
            await _dbContext.SaveChangesAsync();
            // Remove the group if no users in it.
            var any = _dbContext.UserGroupRelations.Any(t => t.GroupId == group.Id);
            if (!any)
            {
                _dbContext.GroupConversations.Remove(group);
                await _dbContext.SaveChangesAsync();
            }
            return this.Protocal(ErrorType.Success, $"你成功退出了这个群聊: {groupName}!");
        }

        [HttpPost]
        public async Task<IActionResult> SetGroupMuted([Required]string groupName, [Required]bool setMuted)
        {
            var user = await GetXiangruiChatUser();
            var group = await _dbContext.GroupConversations.SingleOrDefaultAsync(t => t.GroupName == groupName);
            if (group == null)
            {
                return this.Protocal(ErrorType.NotFound, $"找不到这个群聊: {groupName}!");
            }
            var joined = await _dbContext.GetRelationFromGroup(user.Id, group.Id);
            if (joined == null)
            {
                return this.Protocal(ErrorType.Unauthorized, $"您没有加入这个群聊：{groupName}");
            }
            if (joined.Muted == setMuted)
            {
                return this.Protocal(ErrorType.HasDoneAlready, $"您这个群{group.GroupName}已经 {(joined.Muted ? "静音" : "开启声音")}!");
            }
            joined.Muted = setMuted;
            await _dbContext.SaveChangesAsync();
            return this.Protocal(ErrorType.Success, $"成功设置 {(setMuted ? "静音" : "开启声音")} 在这个群 '{groupName}'!");
        }

        private async Task<XiangruiChatUser> GetXiangruiChatUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            return await _userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}
