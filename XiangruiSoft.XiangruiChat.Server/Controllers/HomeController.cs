﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aiursoft.Pylon;
using Aiursoft.Pylon.Models;
using Microsoft.AspNetCore.Mvc;

namespace XiangruiSoft.XiangruiChat.Server.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Error()
        {
            return this.Protocal(ErrorType.UnknownError, "服务器出现了一些错误!");
        }
    }
}