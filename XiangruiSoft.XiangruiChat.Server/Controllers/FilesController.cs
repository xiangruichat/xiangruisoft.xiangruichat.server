﻿using Aiursoft.Pylon;
using Aiursoft.Pylon.Attributes;
using Aiursoft.Pylon.Models;
using Aiursoft.Pylon.Services;
using Aiursoft.Pylon.Services.ToOSSServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Aiursoft.Pylon.Models.OSS.ApiAddressModels;
using Aiursoft.Pylon.Models.OSS.ApiViewModels;
using XiangruiSoft.XiangruiChat.Server.Data;
using XiangruiSoft.XiangruiChat.Server.Models;
using XiangruiSoft.XiangruiChat.Server.Services;
using XiangruiSoft.XiangruiChat.Server.ViewModels;
using UploadFileAddressModel = XiangruiSoft.XiangruiChat.Server.ViewModels.UploadFileAddressModel;
using UploadFileViewModel = XiangruiSoft.XiangruiChat.Server.ViewModels.UploadFileViewModel;

namespace XiangruiSoft.XiangruiChat.Server.Controllers
{
    [APIExpHandler]
    [APIModelStateChecker]
    [AiurForceAuth(directlyReject: true)]
    public class FilesController : Controller
    {
        private readonly UserManager<XiangruiChatUser> _userManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly ServiceLocation _serviceLocation;
        private readonly StorageService _storageService;
        private readonly AppsContainer _appsContainer;
        private readonly SecretService _secretService;

        public FilesController(
            UserManager<XiangruiChatUser> userManager,
            ApplicationDbContext dbContext,
            IConfiguration configuration,
            ServiceLocation serviceLocation,
            StorageService storageService,
            AppsContainer appsContainer,
            SecretService secretService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _configuration = configuration;
            _serviceLocation = serviceLocation;
            _storageService = storageService;
            _appsContainer = appsContainer;
            _secretService = secretService;
        }

        [HttpPost]
        [FileChecker(MaxSize = 5 * 1024 * 1024)]
        [APIModelStateChecker]
        public async Task<IActionResult> UploadIcon()
        {
            var file = Request.Form.Files.First();
            if (!file.FileName.IsStaticImage())
            {
                return this.Protocal(ErrorType.InvalidInput, "您只能上传图像文件，不可以上传其他文件");
            }
            var uploadedFile = await _storageService.SaveToOSS(file, Convert.ToInt32(_configuration["XiangruiChatUserIconsBucketId"]), 365, SaveFileOptions.RandomName);
            return this.XiangruiJson(new UploadImageViewModel
            {
                Code = ErrorType.Success,
                Message = $"Successfully uploaded your user icon, but we did not update your profile. Now you can call `/auth/{nameof(AuthController.UpdateInfo)}` to update your user icon.",
                FileKey = uploadedFile.FileKey,
                DownloadPath = $"{_serviceLocation.OSS}/Download/FromKey/{uploadedFile.FileKey}"
            });
        }

        [HttpPost]
        [FileChecker]
        [APIModelStateChecker]
        public async Task<IActionResult> UploadMedia()
        {
            var file = Request.Form.Files.First();
            if (!file.FileName.IsImageMedia() && !file.FileName.IsVideo())
            {
                return this.Protocal(ErrorType.InvalidInput, "您只能上传视频和图像文件.");
            }
            var uploadedFile = await _storageService.SaveToOSS(file, Convert.ToInt32(_configuration["XiangruiChatPublicBucketId"]), 30, SaveFileOptions.RandomName);
            return this.XiangruiJson(new UploadImageViewModel
            {
                Code = ErrorType.Success,
                Message = "成功上传媒体文件!",
                FileKey = uploadedFile.FileKey,
                DownloadPath = $"{_serviceLocation.OSS}/Download/FromKey/{uploadedFile.FileKey}"
            });
        }

        [HttpPost]
        [FileChecker]
        [APIModelStateChecker]
        public async Task<IActionResult> UploadFile(UploadFileAddressModel model)
        {
            var conversation = await _dbContext.Conversations.SingleOrDefaultAsync(t => t.Id == model.ConversationId);
            if (conversation == null)
            {
                return this.Protocal(ErrorType.NotFound, $"不能查找这个会话: {model.ConversationId}!");
            }
            var user = await GetXiangruiChatUser();
            if (!await _dbContext.VerifyJoined(user.Id, conversation))
            {
                return this.Protocal(ErrorType.Unauthorized, $"没有授权文件给这个会话: {conversation.Id}!");
            }
            var file = Request.Form.Files.First();
            var uploadedFile = await _storageService.SaveToOSS(file, Convert.ToInt32(_configuration["XiangruiChatPublicBucketId"]), 20, SaveFileOptions.RandomName);
            var fileRecord = new FileRecord
            {
                FileKey = uploadedFile.FileKey,
                SourceName = Path.GetFileName(file.FileName.Replace(" ", "")),
                UploaderId = user.Id,
                ConversationId = conversation.Id
            };
            _dbContext.FileRecords.Add(fileRecord);
            await _dbContext.SaveChangesAsync();
            return this.XiangruiJson(new UploadFileViewModel
            {
                Code = ErrorType.Success,
                Message = "成功上传这个文件!",
                FileKey = uploadedFile.FileKey,
                SavedFileName = fileRecord.SourceName,
                FileSize = file.Length
            });
        }

        [HttpPost]
        public async Task<IActionResult> FileDownloadAddress(FileDownloadAddressAddressModel model)
        {
            var record = await _dbContext
                .FileRecords
                .Include(t => t.Conversation)
                .SingleOrDefaultAsync(t => t.FileKey == model.FileKey);
            if (record == null || record.Conversation == null)
            {
                return this.Protocal(ErrorType.NotFound, "找不到对应的会话!");
            }
            var user = await GetXiangruiChatUser();
            if (!await _dbContext.VerifyJoined(user.Id, record.Conversation))
            {
                return this.Protocal(ErrorType.Unauthorized, $"您没有授权: {record.Conversation.Id}!");
            }
            var secret = await _secretService.GenerateAsync(record.FileKey, await _appsContainer.AccessToken());
            return this.XiangruiJson(new FileDownloadAddressViewModel
            {
                Code = ErrorType.Success,
                Message = "成功获取文件下载地址!",
                FileName = record.SourceName,
                DownloadPath = $"{_serviceLocation.OSS}/Download/FromSecret?Sec={secret.Value}&sd=true&name={record.SourceName}"
            });
        }

        private async Task<XiangruiChatUser> GetXiangruiChatUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            return await _userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}
