﻿using Aiursoft.Pylon;
using Aiursoft.Pylon.Attributes;
using Aiursoft.Pylon.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using XiangruiSoft.XiangruiChat.Server.Data;
using XiangruiSoft.XiangruiChat.Server.Models;
using XiangruiSoft.XiangruiChat.Server.Services;
using XiangruiSoft.XiangruiChat.Server.ViewModels;

namespace Kahla.Server.Controllers
{
    [APIExpHandler]
    [APIModelStateChecker]
    [AiurForceAuth(directlyReject: true)]
    public class FriendshipController : Controller
    {
        private readonly UserManager<XiangruiChatUser> _userManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly PushChatMessageService _pusher;
        private static object _obj = new object();

        public FriendshipController(
            UserManager<XiangruiChatUser> userManager,
            ApplicationDbContext dbContext,
            PushChatMessageService pushService)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _pusher = pushService;
        }

        public async Task<IActionResult> MyFriends([Required]bool? orderByName)
        {
            var user = await GetXiangruiChatUser();
            var list = new List<ContactInfo>();
            var conversations = await _dbContext.MyConversations(user.Id);
            foreach (var conversation in conversations)
            {
                list.Add(new ContactInfo
                {
                    ConversationId = conversation.Id,
                    DisplayName = conversation.GetDisplayName(user.Id),
                    DisplayImageKey = conversation.GetDisplayImage(user.Id),
                    LatestMessage = conversation.GetLatestMessage().Content,
                    LatestMessageTime = conversation.GetLatestMessage().SendTime,
                    UnReadAmount = conversation.GetUnReadAmount(user.Id),
                    Discriminator = conversation.Discriminator,
                    UserId = conversation is PrivateConversation ? (conversation as PrivateConversation).AnotherUser(user.Id).Id : null,
                    AesKey = conversation.AESKey,
                    Muted = conversation is GroupConversation ? (await _dbContext.GetRelationFromGroup(user.Id, conversation.Id)).Muted : false
                });
            }
            list = orderByName == true ?
                list.OrderBy(t => t.DisplayName).ToList() :
                list.OrderByDescending(t => t.LatestMessageTime).ToList();
            return this.XiangruiJson(new AiurCollection<ContactInfo>(list)
            {
                Code = ErrorType.Success,
                Message = "成功获取您所有的好友."
            });
        }

        [HttpPost]
        public async Task<IActionResult> DeleteFriend([Required] string id)
        {
            var user = await GetXiangruiChatUser();
            var target = await _dbContext.Users.FindAsync(id);
            if (target == null)
                return this.Protocal(ErrorType.NotFound, "没有找到对应的好友.");
            if (!await _dbContext.AreFriends(user.Id, target.Id))
                return this.Protocal(ErrorType.NotEnoughResources, "她压根不是您的好友.");
            await _dbContext.RemoveFriend(user.Id, target.Id);
            await _dbContext.SaveChangesAsync();
            await _pusher.WereDeletedEvent(target.Id);
            return this.Protocal(ErrorType.Success, "成功删除好友.");
        }

        [HttpPost]
        public async Task<IActionResult> CreateRequest([Required] string id)
        {
            var user = await GetXiangruiChatUser();
            var target = await _dbContext.Users.FindAsync(id);
            if (target == null)
                return this.Protocal(ErrorType.NotFound, "无法找到好友!");
            if (target.Id == user.Id)
                return this.Protocal(ErrorType.RequireAttention, "不可以向自己发送加好友请求!");
            var areFriends = await _dbContext.AreFriends(user.Id, target.Id);
            if (areFriends)
                return this.Protocal(ErrorType.HasDoneAlready, "你们两个已经是好友了!");
            Request request = null;
            lock (_obj)
            {
                var pending = _dbContext.Requests
                    .Where(t => t.CreatorId == user.Id)
                    .Where(t => t.TargetId == id)
                    .Any(t => !t.Completed);
                if (pending)
                    return this.Protocal(ErrorType.HasDoneAlready, "有一些挂起的请求没有处理完成!");
                request = new Request { CreatorId = user.Id, TargetId = id };
                _dbContext.Requests.Add(request);
                _dbContext.SaveChanges();
            }
            await _pusher.NewFriendRequestEvent(target.Id, user.Id);
            return this.XiangruiJson(new AiurValue<int>(request.Id)
            {
                Code = ErrorType.Success,
                Message = "成功创建了一个好友添加请求!"
            });
        }

        [HttpPost]
        public async Task<IActionResult> CompleteRequest(CompleteRequestAddressModel model)
        {
            var user = await GetXiangruiChatUser();
            var request = await _dbContext.Requests.FindAsync(model.Id);
            if (request == null)
                return this.Protocal(ErrorType.NotFound, "找不到对应的好友添加请求.");
            if (request.TargetId != user.Id)
                return this.Protocal(ErrorType.Unauthorized, "此请求的目标不是您.");
            if (request.Completed == true)
                return this.Protocal(ErrorType.HasDoneAlready, "这个请求目标已经完成.");
            request.Completed = true;
            if (model.Accept)
            {
                if (await _dbContext.AreFriends(request.CreatorId, request.TargetId))
                {
                    await _dbContext.SaveChangesAsync();
                    return this.Protocal(ErrorType.RequireAttention, "你们两个已经是好友了.");
                }
                _dbContext.AddFriend(request.CreatorId, request.TargetId);
                await _pusher.FriendAcceptedEvent(request.CreatorId);
            }
            await _dbContext.SaveChangesAsync();
            return this.Protocal(ErrorType.Success, "成功的完成了请求.");
        }

        public async Task<IActionResult> MyRequests()
        {
            var user = await GetXiangruiChatUser();
            var requests = await _dbContext
                .Requests
                .AsNoTracking()
                .Include(t => t.Creator)
                .Where(t => t.TargetId == user.Id)
                .OrderByDescending(t => t.CreateTime)
                .ToListAsync();
            return this.XiangruiJson(new AiurCollection<Request>(requests)
            {
                Code = ErrorType.Success,
                Message = "成功的获取了您的加好友请求列表."
            });
        }

        public async Task<IActionResult> SearchFriends(SearchFriendsAddressModel model)
        {
            var users = await _dbContext
                .Users
                .AsNoTracking()
                .Where(t => t.NickName.Contains(model.NickName, StringComparison.CurrentCultureIgnoreCase))
                .Take(model.Take)
                .ToListAsync();

            return this.XiangruiJson(new AiurCollection<XiangruiChatUser>(users)
            {
                Code = ErrorType.Success,
                Message = "您的搜索结果已经输出."
            });
        }

        public async Task<IActionResult> DiscoverFriends(int take = 15)
        {
            var cuser = await GetXiangruiChatUser();
            var myfriends = await _dbContext.MyPersonalFriendsId(cuser.Id);
            var calculated = new List<KeyValuePair<int, XiangruiChatUser>>();
            foreach (var user in await _dbContext.Users.ToListAsync())
            {
                if (await _dbContext.AreFriends(user.Id, cuser.Id) || user.Id == cuser.Id)
                {
                    continue;
                }
                var hisfriends = await _dbContext.MyPersonalFriendsId(user.Id);
                var commonFriends = myfriends.Intersect(hisfriends).Count();
                if (commonFriends > 0)
                {
                    calculated.Add(new KeyValuePair<int, XiangruiChatUser>(commonFriends, user));
                }
                if (calculated.Count >= take)
                {
                    break;
                }
            }
            var ordered = calculated.OrderByDescending(t => t.Key);
            return this.XiangruiJson(new AiurCollection<KeyValuePair<int, XiangruiChatUser>>(ordered)
            {
                Code = ErrorType.Success,
                Message = "已经为您推荐了您可能喜欢的好友."
            });
        }

        public async Task<IActionResult> UserDetail([Required]string id)
        {
            var user = await GetXiangruiChatUser();
            var target = await _dbContext.Users.AsNoTracking().SingleOrDefaultAsync(t => t.Id == id);
            var model = new UserDetailViewModel();
            if (target == null)
            {
                model.Message = "没有找到目标用户.";
                model.Code = ErrorType.NotFound;
                return this.XiangruiJson(model);
            }
            var conversation = await _dbContext.FindConversationAsync(user.Id, target.Id);
            if (conversation != null)
            {
                model.AreFriends = true;
                model.ConversationId = conversation.Id;
            }
            else
            {
                model.AreFriends = false;
                model.ConversationId = null;
            }
            model.User = target;
            model.Message = "找出您的用户了.";
            model.Code = ErrorType.Success;
            return this.XiangruiJson(model);
        }

        [HttpPost]
        public async Task<IActionResult> ReportHim(ReportHimAddressModel model)
        {
            var cuser = await GetXiangruiChatUser();
            var targetUser = await _dbContext.Users.SingleOrDefaultAsync(t => t.Id == model.TargetUserId);
            if (targetUser == null)
            {
                return this.Protocal(ErrorType.NotFound, $"没有找到对应的用户： `{model.TargetUserId}`!");
            }
            if (cuser.Id == targetUser.Id)
            {
                return this.Protocal(ErrorType.HasDoneAlready, $"不可以报告自己!");
            }
            // All chedk passed. Report him now!
            _dbContext.Reports.Add(new Report
            {
                TargetId = targetUser.Id,
                TriggerId = cuser.Id,
                Reason = model.Reason
            });
            await _dbContext.SaveChangesAsync();
            return this.Protocal(ErrorType.Success, "成功报告了这个用户!");
        }

        private async Task<XiangruiChatUser> GetXiangruiChatUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            return await _userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}
