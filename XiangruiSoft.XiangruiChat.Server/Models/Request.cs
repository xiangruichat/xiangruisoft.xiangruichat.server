﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace XiangruiSoft.XiangruiChat.Server.Models
{
    public class Request
    {
        [Key]
        public int Id { get; set; }

        public string CreatorId { get; set; }
        [ForeignKey(nameof(CreatorId))]
        public XiangruiChatUser Creator { get; set; }

        public string TargetId { get; set; }
        [ForeignKey(nameof(TargetId))]
        [JsonIgnore]
        public XiangruiChatUser Target { get; set; }

        public DateTime CreateTime { get; set; } = DateTime.UtcNow;
        public bool Completed { get; set; } = false;
    }
}