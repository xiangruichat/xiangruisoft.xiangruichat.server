﻿namespace XiangruiSoft.XiangruiChat.Server.Models
{
    public class Report
    {
        public int Id { get; set; }

        public string TriggerId { get; set; }
        public XiangruiChatUser Trigger { get; set; }

        public string TargetId { get; set; }
        public XiangruiChatUser Target { get; set; }

        public string Reason { get; set; }
        public ReportStatus Status { get; set; } = ReportStatus.Pending;
    }

    public enum ReportStatus
    {
        Pending = 0,
        Resolved = 1
    }
}