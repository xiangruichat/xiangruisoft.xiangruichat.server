﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aiursoft.Pylon.Models.Stargate.ChannelViewModels;
using Aiursoft.Pylon.Services;
using Aiursoft.Pylon.Services.ToStargateServer;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using XiangruiSoft.XiangruiChat.Server.Data;
using XiangruiSoft.XiangruiChat.Server.Events;
using XiangruiSoft.XiangruiChat.Server.Models;

namespace XiangruiSoft.XiangruiChat.Server.Services
{
    public class PushChatMessageService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly PushMessageService _pushMessageService;
        private readonly AppsContainer _appsContainer;
        private readonly ChannelService _channelService;

        public PushChatMessageService(
            ApplicationDbContext dbContext,
            PushMessageService pushMessageService,
            AppsContainer appsContainer,
            ChannelService channelService)
        {
            _dbContext = dbContext;
            _pushMessageService = pushMessageService;
            _appsContainer = appsContainer;
            _channelService = channelService;
        }

        private string _CammalSer(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }

        public async Task<CreateChannelViewModel> Init(string userId)
        {
            var token = await _appsContainer.AccessToken();
            var channel = await _channelService.CreateChannelAsync(token, $"Kahla User Channel for Id:{userId}");
            return channel;
        }

        public async Task NewMessageEvent(string recieverId, int conversationId, string content, XiangruiChatUser sender, string aesKey, bool muted = false, List<XiangruiChatUser> usersTable = null)
        {
            var token = await _appsContainer.AccessToken();
            XiangruiChatUser targetUser = null;
            if (usersTable == null)
            {
                targetUser = await _dbContext.Users.AsNoTracking().SingleOrDefaultAsync(t => t.Id == recieverId);
            }
            else
            {
                targetUser = usersTable.SingleOrDefault(t => t.Id == recieverId);
            }
            var channel = targetUser.CurrentChannel;
            var nevent = new NewMessageEvent
            {
                Type = EventType.NewMessage,
                ConversationId = conversationId,
                Sender = sender,
                Content = content,
                AESKey = aesKey,
                Muted = muted
            };
            if (channel != -1)
                await _pushMessageService.PushMessageAsync(token, channel, _CammalSer(nevent), true);
        }

        public async Task NewFriendRequestEvent(string recieverId, string requesterId)
        {
            var token = await _appsContainer.AccessToken();
            var user = await _dbContext.Users.FindAsync(recieverId);
            var channel = user.CurrentChannel;
            var nevent = new NewFriendRequest
            {
                Type = EventType.NewFriendRequest,
                RequesterId = requesterId
            };
            if (channel != -1)
                await _pushMessageService.PushMessageAsync(token, channel, _CammalSer(nevent), true);
        }

        public async Task WereDeletedEvent(string recieverId)
        {
            var token = await _appsContainer.AccessToken();
            var user = await _dbContext.Users.FindAsync(recieverId);
            var channel = user.CurrentChannel;
            var nevent = new WereDeletedEvent
            {
                Type = EventType.WereDeletedEvent
            };
            if (channel != -1)
                await _pushMessageService.PushMessageAsync(token, channel, _CammalSer(nevent), true);
        }

        public async Task FriendAcceptedEvent(string recieverId)
        {
            var token = await _appsContainer.AccessToken();
            var user = await _dbContext.Users.FindAsync(recieverId);
            var channel = user.CurrentChannel;
            var nevent = new FriendAcceptedEvent
            {
                Type = EventType.FriendAcceptedEvent
            };
            if (channel != -1)
                await _pushMessageService.PushMessageAsync(token, channel, _CammalSer(nevent), true);
        }
    }
}
