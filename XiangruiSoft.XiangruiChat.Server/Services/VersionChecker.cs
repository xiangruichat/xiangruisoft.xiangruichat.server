﻿using System.Threading.Tasks;
using Aiursoft.Pylon.Exceptions;
using Aiursoft.Pylon.Models;
using Aiursoft.Pylon.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace XiangruiSoft.XiangruiChat.Server.Services
{
    public class VersionChecker
    {
        private readonly HTTPService _http;
        private readonly IConfiguration _configuration;
        public VersionChecker(
            HTTPService http,
            IConfiguration configuration
        )
        {
            _http = http;
            _configuration = configuration;
        }

        public async Task<string> Check()
        {
            var url = new AiurUrl(_configuration["ChatMasterPackageJson"], new { });
            var response = await _http.Get(url, false);
            var result = JsonConvert.DeserializeObject<NodePackageJson>(response);
            if (result.Name.ToLower() == "xiangruichat")
            {
                return result.Version;
            }
            else
            {
                throw new AiurUnexceptedResponse(new AiurProtocal()
                {
                    Code = ErrorType.NotFound,
                    Message = "这不是祥瑞Chat的JSON配置文件!"
                });
            }
        }
    }

    public class NodePackageJson
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}