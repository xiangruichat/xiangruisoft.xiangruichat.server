﻿using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class SearchGroupAddressModel
    {
        [MinLength(3)]
        [Required]
        public string GroupName { get; set; }
        public int Take { get; set; } = 20;
    }
}