﻿using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class AuthByPasswordAddressModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}