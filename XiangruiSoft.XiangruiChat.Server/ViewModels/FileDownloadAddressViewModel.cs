﻿using Aiursoft.Pylon.Models;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class FileDownloadAddressViewModel : AiurProtocal
    {
        public string FileName { get; set; }
        public string DownloadPath { get; set; }
    }
}