﻿using System;
using Aiursoft.Pylon.Models;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class UploadImageViewModel : AiurProtocal
    {
        public int FileKey { get; set; }
        public String DownloadPath { get; set; }
    }
}