﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class ReportBugAddressModel
    {
        [Required]
        public String Content { get; set; }
    }
}