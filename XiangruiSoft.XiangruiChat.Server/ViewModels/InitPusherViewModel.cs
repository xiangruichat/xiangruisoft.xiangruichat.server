﻿using Aiursoft.Pylon.Models.Stargate.ChannelViewModels;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class InitPusherViewModel : CreateChannelViewModel
    {
        public string ServerPath { get; set; }
    }
}