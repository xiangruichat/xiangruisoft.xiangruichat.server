﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class UploadFileAddressModel
    {
        [Required]
        [FromQuery]
        public int ConversationId { get; set; }
    }
}