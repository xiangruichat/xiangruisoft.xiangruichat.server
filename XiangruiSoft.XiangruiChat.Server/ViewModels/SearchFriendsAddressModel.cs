﻿using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class SearchFriendsAddressModel
    {
        [MinLength(1)]
        [Required]
        public string NickName { get; set; }

        public int Take { get; set; } = 20;
    }
}