﻿using Aiursoft.Pylon.Models;
using XiangruiSoft.XiangruiChat.Server.Models;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class UserDetailViewModel : AiurProtocal
    {
        public XiangruiChatUser User { get; set; }
        public bool AreFriends { get; set; }
        public int? ConversationId { get; set; }
    }
}