﻿using Aiursoft.Pylon.Models;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class UploadFileViewModel : AiurProtocal
    {
        public string SavedFileName { get; set; }
        public int FileKey { get; set; }
        public long FileSize { get; set; }
    }
}