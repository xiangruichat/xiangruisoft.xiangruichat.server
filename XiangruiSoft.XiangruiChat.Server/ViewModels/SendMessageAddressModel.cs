﻿using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class SendMessageAddressModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Content { get; set; }
    }
}