﻿using System;
using System.ComponentModel.DataAnnotations;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class ReportMessageAddressModel
    {
        [Required]
        public String Content { get; set; }
    }
}