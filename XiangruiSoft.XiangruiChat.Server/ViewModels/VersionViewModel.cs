﻿using Aiursoft.Pylon.Models;

namespace XiangruiSoft.XiangruiChat.Server.ViewModels
{
    public class VersionViewModel : AiurProtocal
    {
        public string LatestVersion { get; set; }
        public string OldestSupportedVersion { get; set; }
        public string DownloadAddress { get; set; }
    }
}